﻿using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

namespace GLLog.Server
{
    public class Start
    {
        #region 公共属性
        /// <summary>
        /// 日志缓存间隔单位毫秒
        /// </summary>
        public static int SendInterval = 5000;
        /// <summary>
        /// 日志缓存数量，开启缓存后当日志数量到达一定值后忽略SendInterval值将日志存储
        /// </summary>
        public static int SendCount = 100;
        /// <summary>
        /// 当客户端未开启日志缓存，服务器是否开启本地日志缓存
        /// </summary>
        public static bool LocalCache = true;
        /// <summary>
        /// 服务器IP
        /// </summary>
        public static string IP = Comms.Comms.GetServerIP();
        /// <summary>
        /// 服务器端口
        /// </summary>
        public static int RemotServerPort = 8419;
        /// <summary>
        /// 日志存储类型
        /// </summary>
        public static StorageType LogType = StorageType.DB;
        /// <summary>
        /// 日志保存类型枚举：DB数据库存储，File文件存储
        /// </summary>
        public enum StorageType
        {
            DB,//DB数据库存储
            File//File文件存储
        };
        /// <summary>
        /// 日志存储在本地机器位置
        /// </summary>
        public static string LogFilePath = @"D:\work\MVC权限管理\Web";
        /// <summary>
        /// 使用RemotServer远程服务器处理时，传输过程加密密钥（设置密钥则加密）
        /// </summary>
        public static string EncrypCode = "GLLog";
        #endregion

        #region 私有属性
        /// <summary>
        /// 日志程序是否在运行
        /// </summary>
        private static bool isRun = false;
        /// <summary>
        /// 缓存处理线程
        /// </summary>
        private static System.Timers.Timer t = null;
        /// <summary>
        /// 监听服务
        /// </summary>
        private static Net.TcpSvr svr;
        #endregion

        #region 启动日志服务
        /// <summary>
        /// 启动日志服务
        /// </summary>
        public static void start()
        {
            if (!isRun)
            {
                try
                {
                    t = new System.Timers.Timer(SendInterval);
                    t.Elapsed += new System.Timers.ElapsedEventHandler(ExcCache);
                    t.AutoReset = true;
                    t.Enabled = true;
                    t.Start();
                    svr = new Net.TcpSvr();
                    svr.Start();
                    isRun = svr.isRun;
                }
                catch (Exception ex)
                {
                    isRun = false;
                    svr.Stop();
                    throw new Exception(ex.Message);
                }
            }
        }
        #endregion

        #region 本地缓存处理
        /// <summary>
        /// 本地缓存处理
        /// </summary>
        private static void ExcCache(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                Comms.SvrWriteLog wl = new Comms.SvrWriteLog();
                if (LogType == StorageType.DB)
                {
                    wl.WirteCacheLogDB();
                }
                else if (LogType == StorageType.File)
                {
                    wl.WirteCacheLogFile();
                }
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }
        }
        #endregion

        #region 获取服务器端连接数
        /// <summary>
        /// 获取服务器端连接数
        /// </summary>
        public static int GetTcpCliNum()
        {
            int result = 0;
            if (svr.isRun)
            {
                result = svr.sessionTable.Count;
            }
            return result;
        }
        #endregion

        #region 停止日志服务
        /// <summary>
        /// 停止日志服务
        /// </summary>
        public static void stop()
        {
            if (!isRun)
            {
                try
                {
                    if (svr.isRun)
                    {
                        svr.Stop();
                    }
                    if (isRun)
                    {
                        t.Stop();
                    }
                    isRun = false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        #endregion
    }
}