﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Text;

namespace GLLog.Client
{
    public static class Start
    {
        #region 公共属性
        /// <summary>
        /// 开启缓存后日志存储间隔单位毫秒
        /// </summary>
        public static int SendInterval = 5000;
        /// <summary>
        /// 程序所在机器IP，如果机器有多个IP地址则以" ; "分割
        /// </summary>
        public static string IP = Comms.Comms.GetIP();
        /// <summary>
        /// 日志缓存数量，开启缓存后当日志数量到达一定值后忽略SendInterval值将日志存储
        /// </summary>
        public static int SendCount = 100;
        /// <summary>
        /// 是否开启本地日志缓存
        /// </summary>
        public static bool LocalCache = true;
        /// <summary>
        /// 远程服务器IP
        /// </summary>
        public static string RemotServerIP = "192.168.70.44";
        /// <summary>
        /// 远程服务器端口
        /// </summary>
        public static int RemotServerPort = 8419;
        /// <summary>
        /// 日志存储类型
        /// </summary>
        public static StorageType LogType = StorageType.DB;
        /// <summary>
        /// 日志保存类型枚举：DB数据库存储，File文件存储，RemotServer远程服务器处理
        /// </summary>
        public enum StorageType
        {
            DB,//DB数据库存储
            File,//File文件存储
            RemotServer//RemotServer远程服务器处理
        };
        /// <summary>
        /// 日志存储在本地机器位置
        /// </summary>
        public static string LogFilePath = @"D:\work\MVC权限管理\Web";
        /// <summary>
        /// 使用RemotServer远程服务器处理时，传输过程是否加密
        /// </summary>
        public static bool Encryp = false;
        /// <summary>
        /// 使用RemotServer远程服务器处理时，传输过程加密密钥（设置密钥则加密）
        /// </summary>
        public static string EncrypCode = "GLLog";
        #endregion
        
        #region 私有属性
        /// <summary>
        /// 日志程序是否在运行
        /// </summary>
        private static bool isRun = false;
        public static object locker = new object();
        /// <summary>
        /// 定时器
        /// </summary>
        private static System.Timers.Timer t = null;
        #endregion

        #region 启动日志服务
        /// <summary>
        /// 启动日志服务
        /// </summary>
        public static void start()
        {
            if (!isRun)
            {
                try
                {
                    t = new System.Timers.Timer(SendInterval);
                    t.Elapsed += new System.Timers.ElapsedEventHandler(SendThrend);
                    t.AutoReset = true;
                    t.Enabled = true;
                    t.Start();
                    isRun = true;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        #endregion

        #region 停止日志服务
        /// <summary>
        /// 停止日志服务
        /// </summary>
        public static void stop()
        {
            if (!isRun)
            {
                try
                {
                    t.Stop();
                    t.Close();
                    isRun = false;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        #endregion

        private static void SendThrend(object source, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (LocalCache)
                {
                    Comms.CliWriteLog wl = new Comms.CliWriteLog();
                    if (LogType == StorageType.DB)
                    {
                        wl.WirteCacheLogDB();
                    }
                    else if (LogType == StorageType.File)
                    {
                        wl.WirteCacheLogFile();
                    }
                    else if (LogType == StorageType.RemotServer)
                    {
                        wl.WirteCacheLogRemotServer();
                    }
                }
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }
        }
    }
}
