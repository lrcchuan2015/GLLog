﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace GLLog.Client
{
    public class BaseLog
    {
        public object locker = new object();

        #region debug
        /// <summary>
        /// debug
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="message">错误描述</param>
        /// <param name="ex">错误信息</param>
        public void Debug(string MIP, string message, Exception ex)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "debug", ex.Source, message, ex.Message, DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "debug", ex.Source, message, ex.Message, DateTime.Now);
            }
        }
        /// <summary>
        /// debug
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="MLogger">错误触发者</param>
        /// <param name="message">错误描述</param>
        public void Debug(string MIP, string MLogger, string message)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "debug", MLogger, message, "", DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "debug", MLogger, message, "", DateTime.Now);
            }
        }
        #endregion

        #region error
        /// <summary>
        /// error
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="message">错误描述</param>
        /// <param name="ex">错误信息</param>
        public void Error(string MIP, string message, Exception ex)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Error", ex.Source, message, ex.Message, DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Error", ex.Source, message, ex.Message, DateTime.Now);
            }
        }

        /// <summary>
        /// error
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="MLogger">错误触发者</param>
        /// <param name="message">错误描述</param>
        public void Error(string MIP, string MLogger, string message)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Error", MLogger, message, "", DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Error", MLogger, message, "", DateTime.Now);
            }
        }
        #endregion

        #region info
        /// <summary>
        /// info
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="message">错误描述</param>
        /// <param name="ex">错误信息</param>
        public void Info(string MIP, string message, Exception ex)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Info", ex.Source, message, ex.Message, DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Info", ex.Source, message, ex.Message, DateTime.Now);
            }
        }

        /// <summary>
        /// info
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="MLogger">错误触发者</param>
        /// <param name="ex">错误描述</param>
        public void Info(string MIP, string MLogger, string message)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Info", MLogger, message, "", DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Info", MLogger, message, "", DateTime.Now);
            }
        }
        #endregion

        #region warn
        /// <summary>
        /// warn
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="message">错误描述</param>
        /// <param name="ex">错误信息</param>
        public void Warn(string MIP, string message, Exception ex)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Warn", ex.Source, message, ex.Message, DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Warn", ex.Source, message, ex.Message, DateTime.Now);
            }
        }

        /// <summary>
        /// warn
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="MLogger">错误触发者</param>
        /// <param name="ex">错误描述</param>
        public void Warn(string MIP, string MLogger, string message)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Warn", MLogger, message, "", DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Warn", MLogger, message, "", DateTime.Now);
            }
        }
        #endregion

        #region fatal
        /// <summary>
        /// fatal
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="message">错误描述</param>
        /// <param name="ex">错误信息</param>
        public void Fatal(string MIP, string message, Exception ex)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Fatal", ex.Source, message, ex.Message, DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Fatal", ex.Source, message, ex.Message, DateTime.Now);
            }
        }

        /// <summary>
        /// fatal
        /// </summary>
        /// <param name="MIP">错误机器IP地址</param>
        /// <param name="MLogger">错误触发者</param>
        /// <param name="ex">错误描述</param>
        public void Fatal(string MIP, string MLogger, string message)
        {
            if (Start.LocalCache)
            {
                PushEntity(MIP, "Fatal", MLogger, message, "", DateTime.Now);
            }
            else
            {
                WriteLog(MIP, "Fatal", MLogger, message, "", DateTime.Now);
            }
        }
        #endregion

        #region 将数据加入缓存
        /// <summary>
        /// 将数据加入缓存
        /// </summary>
        private void PushEntity(string MIP, string MLevel, string MLogger, string MLogMessage, string MException, DateTime MAddTime)
        {
            if (MIP == "")
            {
                MIP = Start.IP;
            }
            lock (locker)
            {
                if (DataEntity.CliData.ee.Count > 0)
                {
                    if (DataEntity.CliData.ee.Find(delegate(DataEntity.CliErrEntity p) { return p.Level == MLevel && p.Logger == MLogger && p.LogMessage == MLogMessage && p.Exception == MException; }) != null)
                    {
                        DataEntity.CliData.ee.Find(delegate(DataEntity.CliErrEntity p) { return p.Level == MLevel && p.Logger == MLogger && p.LogMessage == MLogMessage && p.Exception == MException; }).Count += 1;
                    }
                    else
                    {
                        DataEntity.CliData.ee.Add(new DataEntity.CliErrEntity { IP = MIP, Level = MLevel, Logger = MLogger, LogMessage = MLogMessage, Exception = MException, AddTime = MAddTime, Count = 1, Interval = Start.SendInterval });
                    }
                }
                else
                {
                    DataEntity.CliData.ee.Add(new DataEntity.CliErrEntity { IP = MIP, Level = MLevel, Logger = MLogger, LogMessage = MLogMessage, Exception = MException, AddTime = MAddTime, Count = 1, Interval = Start.SendInterval });
                }
                if (DataEntity.CliData.ee.Count >= Start.SendCount)
                {
                    Comms.CliWriteLog wl = new Comms.CliWriteLog();
                    if (Start.LogType == Start.StorageType.DB)
                    {
                        wl.WirteCacheLogDB();
                    }
                    else if (Start.LogType == Start.StorageType.File)
                    {
                        wl.WirteCacheLogFile();
                    }
                    else if (Start.LogType == Start.StorageType.RemotServer)
                    {
                        wl.WirteCacheLogRemotServer();
                    }
                }
            }
        }
        #endregion

        #region 写非缓存日志
        /// <summary>
        /// 写非缓存日志
        /// </summary>
        private void WriteLog(string MIP, string MLevel, string MLogger, string MLogMessage, string MException, DateTime MAddTime)
        {
            if (MIP == "")
            {
                MIP = Start.IP;
            }
            Comms.CliWriteLog wl = new Comms.CliWriteLog();
            if (Start.LogType == Start.StorageType.DB)
            {
                wl.WirteLogDB(MIP, MLevel, MLogger, MLogMessage, MException, MAddTime);
            }
            else if (Start.LogType == Start.StorageType.File)
            {
                wl.WirteLogFile(MIP, MLevel, MLogger, MLogMessage, MException, MAddTime);
            }
            else if (Start.LogType == Start.StorageType.RemotServer)
            {
                wl.WirteLogRemotServer(MIP, MLevel, MLogger, MLogMessage, MException, MAddTime);
            }
        }
        #endregion
    }
}
