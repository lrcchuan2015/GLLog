﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GLLog.DataEntity
{
    [Serializable]
    public class BaseErrEntity
    {
        /// <summary>
        /// 机器IP地址
        /// </summary>
        public string IP { get; set; }
        /// <summary>
        /// 错误级别
        /// </summary>
        public string Level { get; set; }
        /// <summary>
        /// 报错类
        /// </summary>
        public string Logger { get; set; }
        /// <summary>
        /// 内容
        /// </summary>
        public string LogMessage { get; set; }
        /// <summary>
        /// 错误信息
        /// </summary>
        public string Exception { get; set; }
        /// <summary>
        /// 记录开始时间
        /// </summary>
        public DateTime AddTime { get; set; }
        /// <summary>
        /// 记录间隔
        /// </summary>
        public int Interval { get; set; }
        /// <summary>
        /// 报错计数
        /// </summary>
        public int Count { get; set; }
    }
}
